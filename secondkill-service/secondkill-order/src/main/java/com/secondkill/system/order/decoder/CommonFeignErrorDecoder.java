package com.secondkill.system.order.decoder;

import java.nio.charset.Charset;

import com.secondkill.common.enums.ResultErrEnum;
import com.secondkill.common.expection.AppRuntimeException;
import com.secondkill.common.utils.JsonUtil;
import com.secondkill.common.utils.Result;

import feign.Response;
import feign.Util;
import feign.codec.ErrorDecoder;

public class CommonFeignErrorDecoder extends ErrorDecoder.Default{

    @Override
    public Exception decode(String methodKey, Response response) {
        try {
            String result = Util.toString(response.body().asReader(Charset.defaultCharset()));
            Result exceptionResult = JsonUtil.fromJson(result, Result.class);
            return new AppRuntimeException(exceptionResult.getCode(), exceptionResult.getMsg());
        } catch (Exception e) {
            return new AppRuntimeException(ResultErrEnum.SYSTEM_ERROR);
        }
    }
    
}
