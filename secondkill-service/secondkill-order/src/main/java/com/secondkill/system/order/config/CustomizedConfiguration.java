package com.secondkill.system.order.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.secondkill.system.order.decoder.CommonFeignErrorDecoder;
import feign.codec.ErrorDecoder;

@Configuration
public class CustomizedConfiguration {
    @Bean
    public ErrorDecoder feignDecoder() {
        return new CommonFeignErrorDecoder();
    }
}
