package com.secondkill.system.goods.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.secondkill.common.expection.AppRuntimeException;
import com.secondkill.common.utils.Result;

@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(value = AppRuntimeException.class)
    public ResponseEntity handleServiceException(AppRuntimeException e) {
        return new ResponseEntity(new Result(e.getCode(), e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
