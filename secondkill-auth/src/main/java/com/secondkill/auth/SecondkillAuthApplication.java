package com.secondkill.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@EnableDiscoveryClient
// @EnableFeignClients注解告诉框架扫描所有使用注解@FeignClient定义的feign客户端，并把feign客户端注册到IOC容器中。
@EnableFeignClients("com.secondkill.api.*")
@ComponentScan(basePackages = {"com.secondkill"})
public class SecondkillAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecondkillAuthApplication.class, args);
    }


}
