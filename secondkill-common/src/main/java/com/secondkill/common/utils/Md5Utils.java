package com.secondkill.common.utils;

import org.springframework.util.DigestUtils;

public class Md5Utils {
    public static String encryptToMD5(String text) {
        String md5 = "";
        try {
            md5 = DigestUtils.md5DigestAsHex(text.getBytes("utf-8"));
        } catch (Exception e) {
            // TODO: handle exception
        }
        return md5;
    }
}
